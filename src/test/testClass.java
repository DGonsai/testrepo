package test;

import java.util.Scanner;

public class testClass {
	
	public void findPrimes(int a, int b){
		int counter = 0;
		
		for (int i = a; i < b; i++) {
			boolean isPrimeNumber = true;
			
			for (int j = 2; j < i; j++) {
				if (i % j == 0) {
					isPrimeNumber = false;
					break; // exit the inner for loop
				}
			}
			
			if (isPrimeNumber) {
				counter++;
				System.out.print(i + " is a prime number!!!"+"\n");
			}
		}
	}
	
	public static void main(String[] args) {
		int x, y;
		Scanner sc = new Scanner(System.in);
		x=sc.nextInt();
		y=sc.nextInt();
		testClass tc = new testClass();
		
		
		tc.findPrimes(x, y);
	}

}
